from dal import autocomplete

from django import forms
from history.models import Message, TGUser, Supergroup


class TGUserSearchForm(forms.ModelForm):
    message_text = forms.CharField(label='Search for words or text within a message', widget=forms.TextInput(attrs={'placeholder': 'proposal, assembly, solution'}), required=False)
    user = forms.ModelChoiceField(
        queryset=TGUser.objects.all(),
        widget=autocomplete.ModelSelect2(url='user-autocomplete'),
        label="Filter by user",
        required=False
    )

    group = forms.ModelChoiceField(
        queryset=Supergroup.objects.all(),
        widget=autocomplete.ModelSelect2(url='group-autocomplete'),
        label="Filter by group",
        required=False
    )

    class Meta:
        model = Message
        fields = ()
