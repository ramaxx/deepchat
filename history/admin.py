from django.contrib import admin

from history.models import Message, TGUser

class MessageAdmin(admin.ModelAdmin):
    list_display    = ('date', 'message', 'get_author')
    search_fields = ['message', 'fromid__username']
    list_filter = ['fromid__username']

    def get_author(self, obj):
        return obj.fromid.username
    get_author.short_description = 'From'
    get_author.admin_order_field = 'fromid__username'

admin.site.register(Message, MessageAdmin)
