from django.http import HttpResponse
from django.shortcuts import render

from dal import autocomplete

from history.models import Supergroup, Chat, Message, TGUser
from history.forms import TGUserSearchForm


class UserAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = TGUser.objects.all().order_by('username')

        if self.q:
            qs = qs.filter(username__icontains=self.q)

        return qs

class GroupAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = Supergroup.objects.all().order_by('title')

        if self.q:
            qs = qs.filter(title__icontains=self.q)

        return qs

def index(request, template='history/index.html', page_template='history/message_list.html'):
    if request.is_ajax():
        template = page_template

    messages = Message.objects.filter(contextid__lt=-1000000000000).order_by('-date')

    print(request.GET)
    if request.GET:
        q = request.GET.get('message_text', None)
        if q:
            messages = messages.filter(message__icontains=q)

        user = request.GET.get('user', None)
        if user:
            messages = messages.filter(fromid__id=user)

        group = request.GET.get('group', None)
        if group:
            messages = messages.filter(contextid__id=group)

    msg_count = messages.count()
    supergroups = Supergroup.objects.order_by('?')[:8]
    users = TGUser.objects.order_by('?')[:8]

    form = TGUserSearchForm(request.GET)

    context = {'supergroups': supergroups,
               'messages': messages[:2000],
               'page_template': page_template,
               'msg_count': msg_count,
               'users': users,
               'form': form,
              }
    return render(request, template, context)

def message(request, template='history/message.html', page_template='history/message_list.html'):
    if request.is_ajax():
        template = page_template

    messages = Message.objects.filter(contextid__lt=-1000000000000).order_by('-date')

    print(request.GET)
    if request.GET:
        message = request.GET.get('message_id', None)
        if message:
            messages = messages.filter(id__lte=message)

        user = request.GET.get('user', None)
        if user:
            messages = messages.filter(fromid__id=user)

        group = request.GET.get('group_id', None)
        if group:
            messages = messages.filter(contextid__id=group)

    msg_count = messages.count()

    context = {
               'messages': messages[:2000],
               'page_template': page_template,
               'msg_count': msg_count,
              }
    return render(request, template, context)
