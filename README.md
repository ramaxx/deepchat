DeepChat
========

Deepchat is a Django 2+ app which allows you to search on your Telegram groups what was said, when and by whom.

A new way of navigating the deeply through historical information around your groups.
